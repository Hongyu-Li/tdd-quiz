package domain;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Storage {

    private HashMap<Ticket, Bag> smallStorage = new HashMap<>();
    private HashMap<Ticket, Bag> mediumStorage = new HashMap<>();
    private HashMap<Ticket, Bag> bigStorage = new HashMap<>();
    private HashMap<String, Integer> capacity = new HashMap<>();


    public Storage(int smallCapacity, int mediumCapacity, int bigCapacity) {
        capacity.put("small",smallCapacity);
        capacity.put("medium", mediumCapacity);
        capacity.put("big",bigCapacity);
    }

    public Ticket save(Bag bag) {
        if (bag.getSize().equals("small")){
            if (this.smallStorage.size() < capacity.get(bag.getSize())){
                final Ticket ticket = new Ticket();
                smallStorage.put(ticket,bag);
                return ticket;
            }else{
                throw new StorageIsFullException("Insufficient capacity");
            }
        }
    }

    public Bag retrieve(Ticket ticket) {
        if (!storage.containsKey(ticket)){
            throw new IllegalArgumentException("Invalid Ticket");
        }else{
            final Bag bag = storage.remove(ticket);
            return bag;
        }
    }
}
