package domain;

public class Bag {
    private String size;

    public Bag() {
        this("small");
    }

    public Bag(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }
}
