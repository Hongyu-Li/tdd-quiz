import domain.Bag;
import domain.Storage;
import domain.StorageIsFullException;
import domain.Ticket;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

public class StorageTest {

    static Storage CreateStorageWithPlentyRooms(){
        return new Storage((int) 2L);  //static method will not create instance.
    }

    @Test
    void should_get_ticket_when_saving_a_bag() {
        final Storage storage = CreateStorageWithPlentyRooms();
        final Bag bag = new Bag();

        final Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_a_ticket_when_saving_nothing() {
        final Storage storage = CreateStorageWithPlentyRooms();
        final Bag nothing = null;

        final Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_saved_bag_when_giving_a_valid_ticket()  {
        final Bag bag = new Bag();
        final Storage storage = CreateStorageWithPlentyRooms();
        final Ticket ticket = storage.save(bag);
        final Bag retrieved_bag = storage.retrieve(ticket);
        assertSame(bag,retrieved_bag);  //to be same
    }

    @Test
    void should_get_my_bag_when_giving_my_valid_ticket()  {
        final Bag myBag = new Bag();
        final Bag anotherBag = new Bag();
        final Storage storage = CreateStorageWithPlentyRooms();
        final Ticket myTicket = storage.save(myBag);
        storage.save(anotherBag);
        final Bag retrievedBag = storage.retrieve(myTicket);
        assertSame(myBag,retrievedBag);
    }

    @Test
    void should_get_an_error_message_when_giving_an_wrong_ticket() {
        final Storage storage = CreateStorageWithPlentyRooms();
        storage.save(new Bag());
        final Ticket wrongTicket = new Ticket();
        final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve(wrongTicket));
        assertEquals("Invalid Ticket",illegalArgumentException.getMessage());
    }

    @Test
    void should_get_an_error_message_when_retrieving_bag_from_an_empty_storage() {
        final Storage emptyStorage = CreateStorageWithPlentyRooms();
        final Ticket invalidTicket = new Ticket();
        final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> emptyStorage.retrieve(invalidTicket));
        assertEquals("Invalid Ticket",illegalArgumentException.getMessage());
    }

    @Test
    void should_get_an_error_message_when_retrieving_bag_twice() {
        final Storage storage = CreateStorageWithPlentyRooms();
        final Bag bag = new Bag();
        final Ticket ticket = storage.save(bag);
        final Bag retrievedBag = storage.retrieve(ticket);
        assertSame(bag, retrievedBag);
        final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve(ticket));
        assertEquals("Invalid Ticket",illegalArgumentException.getMessage());
    }

    @Test
    void should_get_nothing_when_giving_a_valid_ticket_but_I_save_nothing(){
        final Storage storage = CreateStorageWithPlentyRooms();
        final Ticket ticket = storage.save(null);
        assertNull(storage.retrieve(ticket));
    }

    @Test
    void should_save_when_saving_a_bag_to_an_empty_storage_whose_capacity_is_2()  {
        final int capacity = 2;
        final Storage storage = new Storage(capacity);
        final Bag bag = new Bag();
        final Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
        final Bag retrievedBag = storage.retrieve(ticket);
        assertSame(bag,retrievedBag);
    }

    @Test
    void should_get_a_ticket_and_an_error_when_saving_two_bags_to_an_empty_storage_whose_capacity_is_1() {
        final int capacity = 1;
        final Storage storage = new Storage(capacity);
        final Ticket ticket = storage.save(new Bag());
        assertNotNull(ticket);
        final StorageIsFullException storageIsFullException = assertThrows(StorageIsFullException.class, () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity",storageIsFullException.getMessage());
    }

    @Test
    void should_get_an_error_when_saving_a_bag_to_an_full_storage_whose_capacity_is_2() {
        final int capacity = 2;
        final Storage storage = new Storage(capacity);
        storage.save(new Bag());
        storage.save(new Bag());
        final StorageIsFullException storageIsFullException = assertThrows(StorageIsFullException.class, () -> storage.save(new Bag()));
        assertEquals("Insufficient capacity",storageIsFullException.getMessage());
    }

    @Test
    void should_get_a_ticket_when_saving_a_bag_after_retrieving_a_bag() {
        final int capacity = 2;
        final Storage storage = new Storage(capacity);
        final Ticket ticket = storage.save(new Bag());
        storage.retrieve(ticket);
        final Ticket otherTicket = storage.save(new Bag());
        assertNotNull(otherTicket);
    }

    @Test
    void should_get_a_ticket_when_saving_a_small_bag_to_a_big_slot() {
        final Bag bag = new Bag();
        final Storage storage = new Storage(smallCapacity, mediumCapacity, bigCapcity);
        final Ticket ticket = storage.save(bag);
        assertNotNull(ticket);

    }
}
